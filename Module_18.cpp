#include <iostream>


class Players_data
{
private:

    int points;
    std::string name;

public:

    Players_data()
    { 
        std::cout << "Input name\n";
        std::cin >> name;

        std::cout << "Input number of points\n";
        std::cin >> points;
    }

    void show_data()
    {
        std::cout << "Name: " << name << " Points: " << points << "\n";
    }

    int get_points()
    {
        return points;
    }
};


void Sort(Players_data* x, int size)
{

    for (int i = 1; i < size; ++i)
    {
        for (int j = size - 1; j >= i; --j)
        {
            if (x[j - 1].get_points() < x[j].get_points())
            {
                Players_data tmp = x[j - 1];
                x[j - 1] = x[j];
                x[j] = tmp;
            }
        }
    }
}


int main()
{
    int number_of_players;

    std::cin >> number_of_players;

    Players_data* p = new Players_data[number_of_players];

    Sort(p, number_of_players);

    for (int i = 0; i < number_of_players; i++)
    {
        p[i].show_data();
    }

    delete []p;

    p = nullptr;
}
